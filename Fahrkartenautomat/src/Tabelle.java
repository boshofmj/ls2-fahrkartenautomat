import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.*;  
 

public class Tabelle {
	
	public static void main(String[] args) throws Exception {
		Scanner key = new Scanner(System.in);
		// arraylists for farenheit and celsius with the given data
		double slope,intersect;
		double sum_c = 0,sum_c_sq = 0,sum_fc = 0;
		int sum_f = 0,sum_f_sq = 0;
		int[] fahrenheit = {-20,-10,0,20,30};
		double[] celsius = {-28.8889,-23.3333, -17.778,-6.6667,-1.1111};
		List<Double> celsius_model = new ArrayList<Double>();
		List<Double> fahrenheit_model = new ArrayList<Double>();
		// reviewed the data and its seems a linear regression can "properly" model the values for celsius
		//Linear regression modelling with the given data give back an input request which gives u every 
		// Calculating all the steps from a least suare linear regression model For example see here: https://towardsdatascience.com/linear-regression-by-hand-ee7fe5a751bf
		for (int i = 0;i < fahrenheit.length;i++) {
			sum_f += fahrenheit[i];
		}
		
		for (int i = 0;i < celsius.length;i++) {
			sum_c += celsius[i];
		}
		
		for (int i = 0;i < celsius.length;i++) {
			double temp_a = fahrenheit[i]*celsius[i];
			sum_fc += temp_a;
		}
		
		for (int i = 0;i < celsius.length;i++) {
			int temp_b = fahrenheit[i]*fahrenheit[i];
			sum_f_sq += temp_b;
		}
		
		slope = ((fahrenheit.length * sum_fc)-(sum_f * sum_c))/((fahrenheit.length * sum_f_sq)- (sum_f * sum_f));
		intersect = (sum_c - slope*sum_f)/fahrenheit.length;
		
	
		System.out.print("The given data\n\n");
		System.out.printf("%-12s|%10s \n", "Fahrenheit", "Celsius");
		for (int i = 0;i < fahrenheit.length;i++) {
			System.out.printf("%-12d|%10.2f\n", fahrenheit[i], celsius[i]);
		}
		System.out.printf("%12s","\n\nWith this given data we fitted a linear model to give u \nback a value for the temperature in celsius for every \ngiven value of fahrenheit in the range of -459,670 F\n(absolute zero) and 10100 F (surface of the sun)");
		System.out.println("\n\nFitted function of the least suare method:\ny = " + slope + " * x " + intersect);
		
		//Modelling the new transformation data from the least square model 
		for (double bae = -459.0;bae < 10100.0000001; bae++) {
			fahrenheit_model.add(bae);
			double result = slope * bae + intersect;
			celsius_model.add(result);
		}
		
		PrintWriter out1 = null;
		out1 = new PrintWriter(new File("output.txt"));
		try {
			for (int i = 0; i < fahrenheit_model.size(); i++) {
				out1.write(fahrenheit_model.get(i)+ ";" + celsius_model.get(i)+ "\n");
			}
		}
		catch (Exception  e) {
			e.printStackTrace();
		}
		
		System.out.println("\n Now its YOUR turn\n GIVE a value for fahrenheit in one value steps for example - 220,0\n\n");
		double result_user = key.nextDouble();
		
		for (int i = 0; i < fahrenheit_model.size(); i++) {
			if (result_user == fahrenheit_model.get(i)) {
				System.out.println("\n\n"+ fahrenheit_model.get(i)+" °F are " + celsius_model.get(i) + "°Celsius");
			}else {
				continue;
			}
		}
		key.close();
		out1.close();
		
		
		
		
	}

}
