import java.util.InputMismatchException;
import java.util.Scanner;

public class RabattSystem {

    public static void main(String[] args){

        boolean status = false;
        double input = 0.0;
        double result = 0.0;

        welcomeMessage();

        try{
            input = getInput();
        }catch (NullPointerException e){
            System.out.println("pls enter only double values, separated by \",\"\n");
        }
        result = calcDiscount(input);
        System.out.printf("Your new price is calculated: %.2f Euro" , result);
    }

    public static void welcomeMessage(){
        System.out.println("Welcome to the discount center!\nEnter the amount of money u have to pay: ");
    }

    public static Double getInput(){
        Scanner key = new Scanner(System.in);
        double temp = 0.0;
        try{
            temp = key.nextDouble();
            if(checkValidValue(temp)){
                return temp;
            }
        }catch(InputMismatchException e){
            System.out.print("ERROR!\n");
        }
        key.close();
        return null;
    }


    public static boolean checkValidValue(Double input){

        if(input >= 0){
            return true;
        }else{
            return false;
        }

    }


    public static Double calcDiscount(Double price){

        if (price >= 0 && price <= 100){
            return price = price * 0.9;
        }else if(price > 100 && price <= 500){
            return price = price * 0.85;
        }else if(price > 500){
            return price = price * 0.8;
        }else{
            System.out.println("Oops something went wrong!");
            return price;
        }
    }



}
