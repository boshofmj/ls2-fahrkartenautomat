import java.util.Scanner;

public class Schaltjahr {

    public static void main(String[] args){

        Scanner key = new Scanner(System.in);

        System.out.println("Willkommen beim Schaltjahr-Rechner\npls enter a year: ");
        int year = 0;
        try{

           year = key.nextInt();

        }catch(NullPointerException e){

            System.out.println("oops, something went wrong");
        }

        checkYear(year);



    }

    public static void checkYear(Integer year){

        if(year < 1582 ){

            if(year % 4 == 0 ){

                System.out.println(year + " ist ein Schaltjahr");
            }else{

                System.out.println(year + " ist kein Schaltjhar");
            }

        }else if(year >= 1582){

            if(year % 4 == 0 ){
                if (year % 100 == 0 && year % 400 != 0){
                    System.out.println(year + " ist kein Schaltjhar");
                }else{
                    System.out.println(year + " ist ein Schaltjahr");
                }


            }

        }

    }


}
