// FAHRKARTENAUTOMAT 
// @Max Boshof

import java.util.Scanner;
import java.util.HashMap;
class Fahrkartenautomat {

	public static void ticketAusgabe(){

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(200);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	public static void printWarenkorb(HashMap<String, Integer> warenkorb){

		System.out.println("Warenkorb: \n");
		System.out.printf("%d Stk. Kurzstrecke AB        2,00 Euro\n", warenkorb.get("KurzAB"));
		System.out.printf("%d Stk. Einzelstrecke AB      3,00 Euro\n", warenkorb.get("EinzelAB"));
		System.out.printf("%d Stk. Tageskarte AB         8,80 Euro\n", warenkorb.get("TagesAB"));
		System.out.printf("%d Stk. 4-Fahrten-Karte AB    9,40 Euro\n", warenkorb.get("VierAB"));
		System.out.println("---------------------------------------");

	}
	public static void welcomeMessage(){

		System.out.println("WILLKOMMN BEIM FAHRKARTENAUTOMAT\n");

	}
	public static Double orderProcess(HashMap<String, Integer> warenkorb){

		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0.0;
		double price;
		int ticketart;
		boolean order_complete = false;
		int foo = -1;
		int bar;

		System.out.print("Waehlen Sie ihre Wunschfahrkarte für Berlin AB aus:\n" +
				"  Kurzstrecke AB [2,00 EUR] --> (1)\n" +
				"  Einzelfahrschein AB [3,00 EUR] --> (2)\n" +
				"  Tageskarte AB [8,80 EUR] --> (3)\n" +
				"  4-Fahrten-Karte AB [9,40 EUR] --> (4)\n\n");

		while(!order_complete){
			boolean process_complete = false;
			System.out.println("Bitte geben Sie die gewuenschte Fahrkartenart an:");
			ticketart = tastatur.nextInt();
			switch(ticketart){
				case 1:
					price =  2.00 * 100;

					while (!process_complete){
						System.out.print("Anzahl Tickets (max. 10 ; min. 1): ");
						bar = warenkorb.get("KurzAB") + tastatur.nextInt();
						warenkorb.put("KurzAB", bar);
						if (warenkorb.get("KurzAB") > 10){
							System.out.println("Ooops something went wrong!!\nDie maximale Anzahl betraegt 10 Tickets.\n");
						}else if(warenkorb.get("KurzAB") < 1){
							System.out.println("Ooops something went wrong!!\nMindestanzahl = 1");
						}else{
							process_complete = true;
						}
					}

					zuZahlenderBetrag += price * warenkorb.get("KurzAB");

					while(foo < 0 || foo > 1){
						System.out.println("Wollen Sie weitere Fahrkarten kaufen? ja = 1 ; nein = 0");
						foo = tastatur.nextInt();
						if(foo == 0){
							order_complete = true;
						}
					}

					foo = -1;
					break;

				case 2:

					price = 3.0 * 100;

					while (!process_complete){
						System.out.print("Anzahl Tickets (max. 10 ; min. 1): ");
						bar = warenkorb.get("EinzelAB") + tastatur.nextInt();
						warenkorb.put("EinzelAB", bar);
						if (warenkorb.get("EinzelAB") > 10){
							System.out.println("Ooops something went wrong!!\nDie maximale Anzahl betraegt 10 Tickets.\n");
						}else if(warenkorb.get("EinzelAB") < 1){
							System.out.println("Ooops something went wrong!!\nMindestanzahl = 1");
						}else{
							process_complete = true;
						}
					}

					zuZahlenderBetrag += price * warenkorb.get("EinzelAB");

					while(foo < 0 || foo > 1){
						System.out.println("Wollen Sie weitere Fahrkarten kaufen? ja = 1 ; nein = 0");
						foo = tastatur.nextInt();
						if(foo == 0){
							order_complete = true;
						}
					}

					foo = -1;
					break;

				case 3:
					price = 8.8 *100;

					while (!process_complete){
						System.out.print("Anzahl Tickets (max. 10 ; min. 1): ");
						bar = warenkorb.get("TagesAB") + tastatur.nextInt();
						warenkorb.put("TagesAB", bar);
						if (warenkorb.get("TagesAB") > 10){
							System.out.println("Ooops something went wrong!!\nDie maximale Anzahl betraegt 10 Tickets.\n");
						}else if(warenkorb.get("TagesAB") < 1){
							System.out.println("Ooops something went wrong!!\nMindestanzahl = 1");
						}else{
							process_complete = true;
						}
					}

					zuZahlenderBetrag += price * warenkorb.get("TagesAB");

					while(foo < 0 || foo > 1){
						System.out.println("Wollen Sie weitere Fahrkarten kaufen? ja = 1 ; nein = 0");
						foo = tastatur.nextInt();
						if(foo == 0){
							order_complete = true;
						}
					}

					foo = -1;
					break;

				case 4:

					price = 9.40 *100;

					while (!process_complete){
						System.out.print("Anzahl Tickets (max. 10 ; min. 1): ");
						bar = warenkorb.get("VierAB") + tastatur.nextInt();
						warenkorb.put("VierAB", bar);
						if (warenkorb.get("VierAB") > 10){
							System.out.println("Ooops something went wrong!!\nDie maximale Anzahl betraegt 10 Tickets.\n");
						}else if(warenkorb.get("VierAB") < 1){
							System.out.println("Ooops something went wrong!!\nMindestanzahl = 1");
						}else{
							process_complete = true;
						}
					}

					zuZahlenderBetrag += price * warenkorb.get("VierAB");

					while(foo < 0 || foo > 1){
						System.out.println("Wollen Sie weitere Fahrkarten kaufen? ja = 1 ; nein = 0");
						foo = tastatur.nextInt();
						if(foo == 0){
							order_complete = true;
						}
					}

					foo = -1;
					break;
			}
		}
		return zuZahlenderBetrag;

	}

	public static void returnMoney(Double zuZahlenderBetrag, Double eingezahlterGesamtbetrag){
		Scanner tastatur = new Scanner(System.in);
		double rueckgabebetrag;
		rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rueckgabebetrag > 0.0) {
			double temp = rueckgabebetrag/100;
			System.out.printf("Der Rueckgabebetrag in Hoehe von %.2f Euro\n", temp);
			System.out.println("wird in folgenden Muenzen ausgezahlt:");

			while (rueckgabebetrag >= 200) { // 2-Euro-Münzen
				System.out.println("2 Euro");
				rueckgabebetrag = rueckgabebetrag - 200;
			}
			while (rueckgabebetrag >= 100) { // 1-Euro-Münzen
				System.out.println("1 Euro");
				rueckgabebetrag = rueckgabebetrag - 100;
			}
			while (rueckgabebetrag >= 50) { // 50-Cent-Münzen
				System.out.println("50 Cent");
				rueckgabebetrag = rueckgabebetrag - 50;
			}
			while (rueckgabebetrag >= 20) { // 20-Cent-Münzen
				System.out.println("20 Cent");
				rueckgabebetrag = rueckgabebetrag - 20;
			}
			while (rueckgabebetrag >= 10) { // 10-Cent-Münzen
				System.out.println("10 Cent");
				rueckgabebetrag = rueckgabebetrag - 10;
			}
			while (rueckgabebetrag >= 5) { // 5-Cent-Münzen
				System.out.println("5 Cent");
				rueckgabebetrag = rueckgabebetrag - 5;
			}
			while (rueckgabebetrag >= 2) { // 2-Cent-Münzen
				System.out.println("2 Cent");
				rueckgabebetrag = rueckgabebetrag - 2;
			}
			while (rueckgabebetrag >= 1) { // 5-Cent-Münzen
				System.out.println("1 Cent");
				rueckgabebetrag = rueckgabebetrag - 1;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wuenschen Ihnen eine gute Fahrt.");

		tastatur.close();
	}

	public static Double innputMoney(Double zuZahlenderBetrag){

		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMuenze;
		double nochZuZahlen = 0.0;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			nochZuZahlen = (zuZahlenderBetrag - eingezahlterGesamtbetrag)/100;
			//System.out.println("---------------------------------------");
			System.out.printf("%33.2f Euro\n" , nochZuZahlen);
			System.out.print("\nEingabe (mind. 5 Cent, hoechstens 200 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble()*100;
			switch((int)eingeworfeneMuenze){

				case 5 -> eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				case 10 ->eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				case 20 ->eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				case 50 -> eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				case 100-> eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				case 200 -> eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				case 500 -> eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				case 1000 -> eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				case 2000 -> eingezahlterGesamtbetrag  +=  eingeworfeneMuenze;
				default -> System.out.println("Not a valid input");

			}
		}
		return eingezahlterGesamtbetrag;

	}

	public static void main(String[] args) {

		// Hashmap init
		HashMap <String, Integer> warenkorb = new HashMap<>();

		warenkorb.put("KurzAB" , 0);
		warenkorb.put("EinzelAB" , 0);
		warenkorb.put("TagesAB" , 0);
		warenkorb.put("VierAB" , 0);

		welcomeMessage();
		// Bestellprozess
		double zuZahlenderBetrag = orderProcess(warenkorb);
		// Anzeige des Warenkorbs
		printWarenkorb(warenkorb);
		// Geldeinwurff
		double eingezahlterGesamtbetrag = innputMoney(zuZahlenderBetrag);
		// Fahrscheinausgabe
		ticketAusgabe();
		// Rückgeldberechnung und -ausgabe
		returnMoney(zuZahlenderBetrag,eingezahlterGesamtbetrag);
	}
}
