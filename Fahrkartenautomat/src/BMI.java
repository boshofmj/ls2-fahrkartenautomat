import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.lang.Math;

public class BMI {

    public static void main(String[] args){


        System.out.println("BMI CALCULATOR\n");

        double height = getHeight();

        double weight = getWeight();

        String gender = getGender();

        double bmi = calcBMI(height,weight);

        checkBMI(bmi,gender);

    }



    public static Double getHeight(){

        Scanner key = new Scanner(System.in);
        double height = 0.0;
        try{
            System.out.println("pls enter ur height in m: ");
            height = key.nextDouble();
        }catch(InputMismatchException e){
            System.out.println("something went wrong with the input");
        }
        return height;
    }

    public static Double getWeight(){

        Scanner key2 = new Scanner(System.in);
        double weight = 0.0;
        try{
            System.out.println("pls enter ur weight in kg: ");
            weight = key2.nextDouble();
        }catch(NoSuchElementException e){
            System.out.println("something went wrong with the input");
        }
        return weight;

    }

    public static String getGender() {

        Scanner key3 = new Scanner(System.in);

        String gender = "";
        try {
            System.out.println("pls enter ur gender male = m , female = f , non-binary = n");
            gender = key3.next();
        } catch (NoSuchElementException e) {
            System.out.println("something went wrong with the input");
        }
        return gender;


    }

    public static Double calcBMI(Double height, Double weight){
        double result = weight / Math.pow(height,2.0);
        System.out.printf("Your BMI = %.2f", result);
        return result;
    }

    public static void checkBMI(Double BMI, String gender){

        if(gender.equals("m")){
            if(BMI > 25){
                System.out.println("your weight is to high!");
            }else if(BMI < 25.0 && BMI >= 20.0){
                System.out.println("your weight is in the normal range!");
            }else{
                System.out.println("your weight is to low!");
            }
        }else if(gender.equals("w")){
            if(BMI > 24){
                System.out.println("your weight is to high!");
            }else if(BMI < 24.0 && BMI >= 19.0){
                System.out.println("your weight is in the normal range!");
            }else{
                System.out.println("your weight is to low!");
            }
        }else if(gender.equals("n")){
            if(BMI > 24.5){
                System.out.println("your weight is to high!");
            }else if(BMI < 24.5 && BMI >= 19.5){
                System.out.println("your weight is in the normal range!");
            }else{
                System.out.println("your weight is to low!");
            }
        }else{
            System.out.println("Not a valid input");
        }
    }


}
