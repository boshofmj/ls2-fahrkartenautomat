import java.util.Scanner;
public class Urlaub {

    /*
    USA: 1 Euro > 0, 98 USD (Dollar)
    Japan: 1 Euro > 141 JPY (Yen)
    England: 1 Euro > 0,88 GBP (Pfund)
    Schweiz: 1 Euro > 0,96 CHF (Franken)
    Dänemark: 1 Euro > 7,44 DKK (Kronen)

     */

    public static double calcAmount(String country, double amount){

        return switch(country){
            case "U", "u" -> amount * 0.98 ;
            case "J", "j" -> amount * 141 ;
            case "E", "e" -> amount * 0.88 ;
            case "S", "s" -> amount * 0.96 ;
            case "D", "d" -> amount * 7.44;
            default -> amount;
        };
    }

    public static String readCountry(Scanner myScanner){

        System.out.println("For which country u wanna exchange ur money?\nType in the first letter of the country to choose it.\n1) Usa (Dollar)\n2) Japan (Yen)\n3) England (Pfund)\n4) Schweiz (Franken)\n5) Dänemark (Kronen)\npls enter ur choice:");
        return myScanner.next();
    }

    public static double readAmount(Scanner myScanner){

        System.out.println("How many euros u wanna exchange: ");
        return myScanner.nextDouble();
    }

    public static double readBudget(Scanner myScanner){

        System.out.println("Please enter ur budget for the trip:");
        return myScanner.nextDouble();
    }

    public static boolean checkBudget(double budget){

        if(budget > (budget * 0.1)){
            return true;
        }else{
            return false;
        }
    }

    public static void showBudget(double budget){
        System.out.println("\nBudget: " + budget +"\n");
    }

    public static double calcNewBudget(double budget, double amount){
        return budget - amount;
    }

    public static void welcomeMessage(){
        System.out.println("Welcome to our exchange hub");
        System.out.println("===========================");
    }

    public static double depositMoney(Scanner myScanner, double budget){
        System.out.println("How much money u wanna deposit?");
        double foo = myScanner.nextDouble();
        return budget + foo;
    }

    public static int mainMenu(Scanner myScanner){
        System.out.println("Main menu\n===========================\n\nCose from one option (u have to choose 1 initially to deposit a budget)\n1) Deposit money\n2) Check budget\n3) Exchange and withdraw money\n4) Exit ");
        System.out.println("\nChoose on option by number:");
        return myScanner.nextInt();

    }

    public static void main(String[] agrs){
        Scanner key = new Scanner(System.in);
        boolean finishedProcess = false;
        double budget = 0.0;
        double amount;

        welcomeMessage();
        while(!finishedProcess){
            int menuChoice = mainMenu(key);
            if(menuChoice == 1){
                budget = depositMoney(key, budget);
            }else if(menuChoice == 2){
                showBudget(budget);
            }else if(menuChoice == 3){
                    String country = readCountry(key);
                    amount = readAmount(key);
                    if(checkBudget(calcNewBudget(budget,amount))){
                        switch(country){
                            case "u", "U" ->  {
                                System.out.printf("U withdrawed %.2f in US-Dollar", calcAmount(country,amount));
                                budget = calcNewBudget(budget, amount);
                                showBudget(budget);
                            }
                            case "J", "j" ->  {
                                System.out.printf("U withdrawed %.2f in Yen", calcAmount(country,amount));
                                budget = calcNewBudget(budget, amount);
                                showBudget(budget);
                            }
                            case "e", "E" -> {
                                System.out.printf("U withdrawed %.2f in Pfund", calcAmount(country, amount));
                                budget = calcNewBudget(budget, amount);
                                showBudget(budget);
                            }
                            case "D", "d" ->  {
                                System.out.printf("U withdrawed %.2f in Kronen", calcAmount(country,amount));
                                budget = calcNewBudget(budget, amount);
                                showBudget(budget);
                            }
                            case "S", "s" ->  {
                                System.out.printf("U withdrawed %.2f in Franken", calcAmount(country,amount));
                                budget = calcNewBudget(budget, amount);
                                showBudget(budget);
                            }
                        }
                }
            }else if(menuChoice == 4){
                finishedProcess = true;
            }else{
                System.out.println("not a valid choice");
                continue;
            }
        }

        // INPUT


        // PROCESSING

        // OUTPUT


        //DEBUG



        key.close();
    }

}
