
public class forExercise {

    public static void main (String[] args){

        for1();
        for2();


    }

    public static void for1(){

        for(int i = 1; i <= 10;i++){
            System.out.printf("%d\n", i*i);
        }

    }

    public static void for2(){

        for(int i = 10; i <= 20;i++){
            if(i == 11){
               continue;
            }else if(i == 18){
                break;
            }else{
                System.out.printf("%d\n", i*i);
            }

        }

    }

}
