import java.util.Scanner;

public class Volumen {


    public static void main (String[] args){

        double x;
        double result;
        welcomeMessage();
        x = input();
        result = calcResult(x);
        printResult(result);

    }

    public static void welcomeMessage(){

        System.out.println("Willkommen beim 'GET UR VOLUME v1.0");

    }

    public static void printResult(double result){

        System.out.println("Das Volumen des Würfels betraegt: " + result + "m2");

    }

    public static double calcResult(double x){

        return x*x*x;

    }
    public static double input(){

        Scanner key = new Scanner(System.in);
        System.out.println("Bitte gib die Werte fuer die Seite x des Wuerfels an");

        double x = key.nextDouble();

        return x;

    }


}
