public class ArrayUebung {


  public static void main(String args[]) {
    // 1. Deklaration eines Arrays für ganze Zahlen.
      
    int []  ganzeZahlen = {};
    
    // 2. Initialisierung des Arrays mit 100 Elementen.
    
    int [] ganzenZahlen = new int[100];
    
    
    // 3. Durchlaufen des gesamten Arrays und Ausgabe des Inhalts.
    int [] test = {1,2,3};

    for (int i = 0;i < test.length;i++) {

        System.out.println(test[i]);
    
    }

    // 4. Das Array mit allen ganzen Zahlen von 1 bis 100 befüllen.

    for (int n = 0;n <= 99; n ++){

    ganzenZahlen[n] = n;

    }

    for (int i = 0;i < ganzenZahlen.length;i++) {

        System.out.println(ganzenZahlen[i]);
    
    }


    // 5. Geben Sie den Wert an der 89. Position des Arrays aus.          
    
    System.out.println("arraywert nr.89 = " + ganzenZahlen[89]);

    // 6. Ändern Sie den Wert des Arrayelements mit dem Index 49 zu 1060.

    ganzenZahlen[49] = 1060;

    // Außerdem ändern Sie den Wert an der ersten und der letzte Stelle des Arrays zu 2023.

    ganzenZahlen[ganzenZahlen.length - 1] = 2023;
    // 7. Erneutes Ausgeben des Arrayinhalts.  Darstellung: Index und zugehöriger Inhalt (z.B. Index 6 - Inhalt: 7)

    for (int i = 0; i < ganzenZahlen.length; i++){
      System.out.printf("\n Index %d - Inhalt %d", i , ganzenZahlen[i]);
    }
    // 8. Berechnung des Durchschnitts aller Arrayelemente
    int result = 0;
   for (int i = 0; i < ganzenZahlen.length; i++ ){
      
     result += ganzenZahlen[i];

   }

   int mean = result / ganzenZahlen.length;
   System.out.println("\nDurchschnitt = " + result);
  }
  

}
